from setuptools import setup


with open('README.md', 'rb') as f:
    longdesc = f.read().decode().strip()


setup(
    setup_requires=[
        'setuptools_scm',
    ],

    use_scm_version={
        'write_to': 'arx/__version__.py',
    },

    name='arx',
    description="Simple data archiving tool",
    long_description=longdesc,
    long_description_content_type='text/markdown',
    author='Jameson Graef Rollins',
    author_email='jameson.rollins@ligo.org',
    url='https://git.ligo.org/jameson.rollins/arx',
    license='GPL-3.0-or-later',
    classifiers=[
        ('License :: OSI Approved :: '
         'GNU General Public License v3 or later (GPLv3+)'),
        'Development Status :: 5 - Production/Stable',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],

    install_requires=[
        'pyyaml',
    ],

    packages=[
        'arx',
    ],

    entry_points={
        'console_scripts': [
            'arx = arx.__main__:main',
        ],
    },
)
